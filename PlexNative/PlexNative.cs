//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------


public class PlexNative {
  public static SWIGTYPE_p_int32_t None {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.None_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Letters {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Letters_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Words {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Words_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Regular {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Regular_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Bold {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Bold_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Italic {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Italic_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Underline {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Underline_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Strikeout {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Strikeout_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t TopLeft {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.TopLeft_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Top {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Top_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t TopRight {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.TopRight_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Left {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Left_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Middle {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Middle_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Right {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Right_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t BottomLeft {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.BottomLeft_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t Bottom {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.Bottom_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int32_t BottomRight {
    get {
      SWIGTYPE_p_int32_t ret = new SWIGTYPE_p_int32_t(PlexNativePINVOKE.BottomRight_get(), true);
      return ret;
    } 
  }

  public static SWIGTYPE_p_int64_t MeasureString(string text, SWIGTYPE_p_int32_t textlen, string typeface, SWIGTYPE_p_int32_t typefacelen, double pointsize, SWIGTYPE_p_int32_t styles, SWIGTYPE_p_int32_t alignment, SWIGTYPE_p_int32_t wrapmode, SWIGTYPE_p_int32_t wrapwidth) {
    SWIGTYPE_p_int64_t ret = new SWIGTYPE_p_int64_t(PlexNativePINVOKE.MeasureString(text, SWIGTYPE_p_int32_t.getCPtr(textlen), typeface, SWIGTYPE_p_int32_t.getCPtr(typefacelen), pointsize, SWIGTYPE_p_int32_t.getCPtr(styles), SWIGTYPE_p_int32_t.getCPtr(alignment), SWIGTYPE_p_int32_t.getCPtr(wrapmode), SWIGTYPE_p_int32_t.getCPtr(wrapwidth)), true);
    if (PlexNativePINVOKE.SWIGPendingException.Pending) throw PlexNativePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static void DrawString(string text, SWIGTYPE_p_int32_t textlen, string typeface, SWIGTYPE_p_int32_t typefacelen, double pointsize, SWIGTYPE_p_int32_t styles, SWIGTYPE_p_int32_t alignment, SWIGTYPE_p_int32_t wrapmode, SWIGTYPE_p_int32_t wrapwidth, double r, double g, double b, double a, SWIGTYPE_p_int32_t w, SWIGTYPE_p_int32_t h, SWIGTYPE_p_unsigned_char buffer) {
    PlexNativePINVOKE.DrawString(text, SWIGTYPE_p_int32_t.getCPtr(textlen), typeface, SWIGTYPE_p_int32_t.getCPtr(typefacelen), pointsize, SWIGTYPE_p_int32_t.getCPtr(styles), SWIGTYPE_p_int32_t.getCPtr(alignment), SWIGTYPE_p_int32_t.getCPtr(wrapmode), SWIGTYPE_p_int32_t.getCPtr(wrapwidth), r, g, b, a, SWIGTYPE_p_int32_t.getCPtr(w), SWIGTYPE_p_int32_t.getCPtr(h), SWIGTYPE_p_unsigned_char.getCPtr(buffer));
    if (PlexNativePINVOKE.SWIGPendingException.Pending) throw PlexNativePINVOKE.SWIGPendingException.Retrieve();
  }

}
